package aoc0112;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class Ex1_1 {

    private int frequency;

    // Verkeerd ( te hoog ) : 80339
    //                        171506

    public static void main(String[] args){

        try{

            Ex1_1 ex11 = new Ex1_1();

            FileInputStream fstream = new FileInputStream("input/0112-01.txt");

            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            String strLine;

            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                ex11.modFrequency(strLine);
            }

            ex11.printFrequency();
        } catch (Exception e) {
            System.out.println("File not found");
            System.exit(200);
        }

    }

    private void modFrequency(String strLine) {
        // cut string
        String operation = strLine.substring(0, 1);
        int modifier = Integer.parseInt(strLine.substring(1));

        if(operation.equals("-")){
            this.frequency -= modifier;
        } else {
            this.frequency += modifier;
        }
    }

    private void printFrequency(){
        System.out.println(this.frequency);
    }
}
