package aoc0112;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Ex1_2 {

    private int frequency = 0;
    private ArrayList<Integer> previous = new ArrayList<>();
    private ArrayList<Integer> input = new ArrayList<>();

    public static void main(String[] args) {

        Ex1_2 ex11 = new Ex1_2();
        ex11.init();
        ex11.parse();

    }

    private void init() {
        try {
            FileInputStream xfstream = new FileInputStream("input/0112-01.txt");

            BufferedReader xbr = new BufferedReader(new InputStreamReader(xfstream));

            String xstrLine;

            while ((xstrLine = xbr.readLine()) != null) {
                // Print the content on the console
                this.addToInput(xstrLine);
            }

        } catch (Exception e) {
            System.out.println("Init of input array went wrong");
        }
    }

    private void addToInput(String xstrLine) {
        String operation = xstrLine.substring(0, 1);
        int modifier = Integer.parseInt(xstrLine.substring(1));

        if (operation.equals("-")) {
            input.add(0 - modifier);
        } else {
            input.add(modifier);
        }
    }

    private void parse() {

        while(true){
            for (Integer num: input
                 ) {
                modFrequency(num);
                isCurFrequencyAlreadyBeenSet();
            }
        }

    }


    private void isCurFrequencyAlreadyBeenSet() {
        if (this.previous.contains(this.frequency)) {
            System.out.println(this.frequency);
            System.exit(0);
        }
    }

    private void modFrequency(int number) {
        // cut string
        previous.add(this.frequency);
        this.frequency += number;
    }

    private void printFrequency() {
        System.out.println("Total: " + this.frequency);
    }

}