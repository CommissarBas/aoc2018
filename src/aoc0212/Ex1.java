package aoc0212;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class Ex1 {
    // Te hoog:
    // 19406
    // 116436
    // 19406

    private ArrayList<String> boxes;
    private int threes;
    private int twos;

    public Ex1() {
        this.boxes = new ArrayList<>();
        this.threes = 0;
        this.twos = 0;
    }

    public static void main(String[] args) {
        Ex1 ex = new Ex1();
        ex.init();
        ex.parseBoxes();
        ex.getChecksum();
        ex.getCommonLetters();
    }

    private void getCommonLetters() {
        // Part 2
        for(int i = 0; i<boxes.size(); i++){
            String currBox = boxes.get(i);
            for(String code: boxes){

                // Make sure we don't match ourselves
                if(!code.equals(currBox)){
                    ArrayList<Character> diff = new ArrayList<>();
                    for(int z = 0; z < currBox.length(); z++){
                        if(currBox.charAt(z) != code.charAt(z)){
                            diff.add(currBox.charAt(z));

                        }
                    }

                    if(diff.size() == 1){
                        System.out.println(currBox.replace(diff.get(0).toString(), ""));
                    }
                }
            }
        }
    }

    private void getChecksum() {
        System.out.println(threes * twos);
    }

    private void parseBoxes() {
        for (String boxid: boxes
             ) {
            this.parseBox(boxid);
        }
    }

    private void parseBox(String boxid) {
        ArrayList<Character> charactercounter = new ArrayList<>();
        Boolean localTwo = false;
        Boolean localThree = false;

        for (int i = 0; i < boxid.length(); i++) {
            if (!charactercounter.contains(boxid.charAt(i))) {
                int count = boxid.length() - boxid.replaceAll(Character.toString(boxid.charAt(i)), "").length();

                if (count == 2 && !localTwo) {
                    twos++;
                    localTwo  = true;
                }

                if (count == 3 && !localThree) {
                    threes++;
                    localThree = true;
                }

                charactercounter.add(boxid.charAt(i));
            }
        }
    }

    private void init() {

        try {

            FileInputStream fstream = new FileInputStream("input/0212-01.txt");

            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

            String strLine;

            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                this.boxes.add(strLine);
            }
        } catch (Exception e) {
            System.out.println("Failed to parse input file");
        }
    }
}
