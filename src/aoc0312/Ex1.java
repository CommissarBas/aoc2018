package aoc0312;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Ex1 {

    // Te hoog: 522046, 363040, 121521

    private List<ArrayList<String>> cloth;
    private List<String> input;

    public Ex1() {
        this.cloth = new ArrayList<>();
        this.input = new ArrayList();
        this.createCloth();
    }

    private void createCloth() {
        for(int i = 0; i < 1000; i++){
            cloth.add(new ArrayList<>());
            for(int y = 0; y < 1000; y++){
                cloth.get(i).add(".");
            }
        }
    }

    private void readFile() {
        try {
            FileInputStream fstream = new FileInputStream("input/0312-01.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;

            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                this.input.add(strLine);
            }
        } catch (Exception e) {
            System.out.println("Failed to parse input file");
        }
    }

    private void processInput() {
        for (String line: input
             ) {
            // Commented for visibility...
            StringTokenizer tinput = new StringTokenizer(line);

            // Get claim id
            String claim = tinput.nextToken().substring(1);

            // Discard @, this is nasty me thinks...
            tinput.nextToken();

            // Get coordinates
            String coords = tinput.nextToken();
            coords = coords.replace(":", "");
            String[] coordinates = coords.split(",");

            // Get size
            String size = tinput.nextToken();
            String[] clothSize = size.split("x");


            // moar hack
            int miny = Integer.parseInt(coordinates[1]);
            int maxy = miny + Integer.parseInt(clothSize[1]);

            int minx = Integer.parseInt(coordinates[0]);
            int maxx = minx + Integer.parseInt(clothSize[0]);

            for(int y = miny; y < maxy; y++){
                for(int x = minx; x < maxx; x++){
                    if(cloth.get(y).get(x).equals(".")){
                        cloth.get(y).set(x, claim);
                    }else if(!cloth.get(y).get(x).equals(".")){
                        cloth.get(y).set(x, "#");
                    }
                }
            }
        }
    }

    private void countDuplicates() {
        int counter = 0;
        for (ArrayList<String> array: cloth
             ) {
            for (String value: array
                 ) {
                if(value.equals("#")){
                    counter++;
                }
            }
        }

        System.out.println(counter);
    }

    private void uniqueSelection() {
        ArrayList<String> uniques = new ArrayList<>();

        for (String line : input
        ) {

            Boolean unique = true;

            // Commented for visibility...
            StringTokenizer tinput = new StringTokenizer(line);

            // Get claim id
            String claim = tinput.nextToken().substring(1);

            // Discard @, this is nasty me thinks...
            tinput.nextToken();

            // Get coordinates
            String coords = tinput.nextToken();
            coords = coords.replace(":", "");
            String[] coordinates = coords.split(",");

            // Get size
            String size = tinput.nextToken();
            String[] clothSize = size.split("x");


            // moar hack
            int miny = Integer.parseInt(coordinates[1]);
            int maxy = miny + Integer.parseInt(clothSize[1]);

            int minx = Integer.parseInt(coordinates[0]);
            int maxx = minx + Integer.parseInt(clothSize[0]);


            for (int y = miny; y < maxy; y++) {
                for (int x = minx; x < maxx; x++) {
                    if (!cloth.get(y).get(x).equals(claim)) {
                        unique = false;
                    }
                }

            }

            if(unique){
                uniques.add(claim);
            }
        }

        System.out.println(uniques);
    }

    public static void main(String[] args) {
        Ex1 ex = new Ex1();
        ex.readFile();
        ex.processInput();
        ex.countDuplicates();
        ex.uniqueSelection();
    }

}
