package aoc0412;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

// Too low: 21923, 29895
// Too low: (ex 2) 79167

public class Ex {
    private Map<LocalDateTime,String> guardlog;
    private SortedSet<LocalDateTime> sortedLog;

    private int ranzigsteVariabeleOoit;

    public Ex() {
        this.guardlog = new HashMap<>();
    }

    public static void main(String[] args) {
        Ex ex = new Ex();
        ex.initAndSort();
        ex.getLazyGuardAndMinute();

        // Ex 2!
        // Does not work, correct answer is 2137 * 50, but that is for some reason not calculated
        // Figured that out by manually inspecting the overviewpm object :-)
        // Maybe fix it later, probably won't
        ex.getLazyGuardOnSameMinute();
    }

    public void initAndSort() {
        try {

            FileInputStream fstream = new FileInputStream("input/0412-01.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;                                          //1518-11-14 00:00
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

            while ((strLine = br.readLine()) != null) {
                // Print the content on the console
                String[] input = strLine.split("] ");
                LocalDateTime date = LocalDateTime.parse(input[0].substring(1), formatter);
                String logentry = input[1];
                this.guardlog.put(date, logentry);
            }

            this.sortedLog = new TreeSet<>(guardlog.keySet());

        } catch (Exception e) {
            System.out.println("Failed to parse input file");
        }
    }

    private void getLazyGuardAndMinute() {
        int guard;
        int minute;
        guard = this.GetLazyGuard();
        minute = this.getLazyMinute(guard);

        System.out.println(guard + " x " + minute + " = " + guard * minute);
    }

    private int GetLazyGuard() {
        HashMap<Integer, Long> sleepyGuards = new HashMap<>();

        int currguard = 0;
        LocalDateTime startsleep = LocalDateTime.now();
        for (LocalDateTime entry: this.sortedLog
             ) {

            String line = this.guardlog.get(entry);

            if(line.contains("begins")){
                currguard = parseGuardId(line);
            } else if(line.contains("fall")){
                startsleep = entry;
            } else if(line.contains("wakes")){
                if(sleepyGuards.keySet().contains(currguard)){
                    sleepyGuards.put(currguard, sleepyGuards.get(currguard) + startsleep.until(entry, ChronoUnit.MINUTES));
                } else {
                    sleepyGuards.put(currguard, startsleep.until(entry, ChronoUnit.MINUTES));
                }
            }

        }

        int laziestGuard = 0;
        long maxSleep = 0;

        for (int guard: sleepyGuards.keySet()
             )
            if (sleepyGuards.get(guard) > maxSleep) {
                maxSleep = sleepyGuards.get(guard);
                laziestGuard = guard;
            }

        return laziestGuard;
    }

    private int parseGuardId(String line) {
        for(String section: line.split(" ")){
            if(section.contains("#")){
                return Integer.parseInt(section.replace("#", ""));
            }
        }

        return 0;
    }

    private int getLazyMinute(int guard) {
        HashMap<Integer, Integer> timetable = new HashMap<>();
        LocalDateTime startsleep = LocalDateTime.now();

        for (int i = 0; i < 60; i++) {
            timetable.put(i, 0);
        }

        int currguard = 0;

        for (LocalDateTime entry : this.sortedLog
        ) {
            String line = this.guardlog.get(entry);

            if (line.contains("begins")) {
                currguard = parseGuardId(line);
            } else if (line.contains("fall") && currguard == guard) {
                startsleep = entry;
            } else if (line.contains("wakes") && currguard == guard) {
                for (int i = startsleep.getMinute(); i < entry.getMinute(); i++) {
                    timetable.put(i, timetable.get(i) + 1);
                }
            }

        }

        int maxint = -1;
        int rightMinute = 0;

        for(Integer minute: timetable.keySet()){
            if(timetable.get(minute) > maxint){
                maxint = timetable.get(minute);
                rightMinute = minute;
            }
        }

        return rightMinute;
    }

    private void getLazyGuardOnSameMinute() {
        HashMap<Integer, HashMap<Integer, Integer>> overviewpm = new HashMap<>();
        Set<Integer> allGuards = getSetOfAllGuards();
        HashMap<Integer, Integer> thisTimetable = new HashMap<>();

        for(Integer guard: allGuards){
            overviewpm.put(guard, getLazyMinutesForGuard(guard));
        }

        int guard = 0;
        int minute = -1;
        int total = 0;
        int temp = 0;

        for (int timetable: overviewpm.keySet()
             )
            temp = timetable;
            thisTimetable = overviewpm.get(temp);

            for (Integer currMinute: thisTimetable.keySet()
                 ) {
                if(thisTimetable.get(currMinute) > total){
                    total = thisTimetable.get(currMinute);
                    guard = temp;
                    minute = currMinute; // 2137 * 50
                }
            }

        System.out.println(guard + " x " + minute + " = " + guard * minute);
    }

    private HashMap<Integer, Integer> getLazyMinutesForGuard(int guard) {
        HashMap<Integer, Integer> timetable = new HashMap<>();
        LocalDateTime startsleep = LocalDateTime.now();

        for (int i = 0; i < 60; i++) {
            timetable.put(i, 0);
        }

        int currguard = 0;

        for (LocalDateTime entry : this.sortedLog
        ) {
            String line = this.guardlog.get(entry);

            if (line.contains("begins")) {
                currguard = parseGuardId(line);
            } else if (line.contains("fall") && currguard == guard) {
                startsleep = entry;
            } else if (line.contains("wakes") && currguard == guard) {
                for (int i = startsleep.getMinute(); i < entry.getMinute(); i++) {
                    timetable.put(i, timetable.get(i) + 1);
                }
            }

        }

        return timetable;
    }

    private Set<Integer> getSetOfAllGuards() {
        HashSet<Integer> allGuards = new HashSet<>() ;
        for (LocalDateTime entry : this.sortedLog
        ) {
            String line = this.guardlog.get(entry);

            if (line.contains("begins")) {
                allGuards.add(parseGuardId(line));
            }
        }

        return allGuards;
    }
}
