package aoc0512;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Ex {
    private String polymer;
    private ArrayList<String> components;

    // Too high: 27734, 27102

    public static void main(String[] args) {
        Ex ex = new Ex();

        ex.init();
        System.out.println(ex.parse());

        ex.improvePolymer();
    }

    private void init() {
        try {
            FileInputStream fstream = new FileInputStream("input/0512-01.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            polymer = br.readLine();
        } catch (Exception e) {
            System.out.println("Failed to parse input file");
        }
    }

    private int parse() {
        this.components = new ArrayList<>(Arrays.asList(polymer.split("")));
        Boolean changed = true;

        while(changed){
            changed = false;


            for(int i = 0; i < components.size() - 1; i++){
                if((components.get(i).equals(components.get(i + 1).toUpperCase()) ||
                        components.get(i).equals(components.get(i + 1).toLowerCase())) &&
                        !components.get(i).equals(components.get(i + 1))){
                    components.remove(i+1);
                    components.remove(i);
                    changed = true;
                }
            }
        }

        return components.size();
    }

    private void improvePolymer() {
        HashMap<String, Integer> score = new HashMap<>();
        for(String letter: "abcdefghijklmnopqrstuvwxyz".split("")){
            this.init();
            this.polymer = this.polymer.replace(letter, "");
            this.polymer = this.polymer.replace(letter.toUpperCase(), "");

            this.components = new ArrayList<>(Arrays.asList(polymer.split("")));

            int count = this.parse();

            score.put(letter, count);
        }

        int smallest = Integer.MAX_VALUE;

        for(Integer size: score.values()){
            if(size < smallest){
                smallest = size;
            }
        }

        System.out.println(smallest);

    }
}
