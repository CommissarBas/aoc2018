package aoc0612;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.StrictMath.abs;

public class Ex {
    private ArrayList<String> inputCoords;
    private ArrayList<ArrayList<String>> grid;

    public Ex() {
        this.inputCoords = new ArrayList<>();
    }

    private void init() {
        try {
            FileInputStream fstream = new FileInputStream("input/0612-01.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while((strLine = br.readLine()) != null){
                inputCoords.add(strLine);
            }
        } catch (Exception e) {
            System.out.println("Failed to parse input file");
        }
    }

    private void createGrid(){
        int maxXint = getMaxint(0);
        int maxYint = getMaxint(1);

        this.grid = new ArrayList<>();

        for(int y = 0; y <= maxYint; y++) {
            grid.add(new ArrayList<String>());
            for (int x = 0; x < maxXint; x++) {
                grid.get(y).add("X");
            }
        }
    }

    private int getMaxint(int xory) {
        int maxX = Integer.MIN_VALUE;
        for(String X_coords: inputCoords){
            if(Integer.parseInt(X_coords.split(", ")[xory]) > maxX){
                maxX = Integer.parseInt(X_coords.split(", ")[xory]);
            }
        }
        return maxX;
    }

    private void placeCoords() {

        int coord = 1;

        for(String coords: inputCoords){

            int x = Integer.parseInt(coords.split(", ")[0]);
            int y = Integer.parseInt(coords.split(", ")[1]);

            grid.get(y).add(x, Integer.toString(coord));
            coord++;
        }
    }

    private void manhattanProject(){

        for(int y = 0; y < grid.size(); y++){
            for(int x = 0; x < grid.get(y).size(); x++){
                String closest = checkClosestHub(y, x);
                if(closest.equals("-1")) {
                    grid.get(y).set(x, ".");
                } else {
                    grid.get(y).set(x, closest);
                }
            }
        }

        getBiggestSurface();
    }

    private void getBiggestSurface() {
        HashMap<Integer, ArrayList<Integer>> surfaces = new HashMap<>();
        int coord = 1;

        for (String input: inputCoords
             ) {
            surfaces.put(coord, new ArrayList<>());
            surfaces.get(coord).add(0);
            coord++;
        }

        for(int y = 0; y < grid.size(); y++){
            for(int x = 0; x < grid.get(y).size() -1; x++){
                String found = grid.get(y).get(x);
                if(found.equals(grid.get(y).get(x + 1))
                        && found.equals(grid.get(y+1).get(x))){
                    surfaces.get(found).set(surfaces.get(Integer.parseInt(found)).size() -1,
                            surfaces.get(Integer.parseInt(found)).get(surfaces.get(Integer.parseInt(found)).size() -1) + 1);
                } else if (found.equals(".")){
                    System.out.println("Skipping dot");
                } else {
                    try {
                        surfaces.get(Integer.parseInt(found)).add(0);
                    } catch (NullPointerException e){
                        System.out.println("Missed: " + found);
                    }

                }
            }
        }

        System.out.println("Break here");

    }

    private String checkClosestHub(int y, int x) {
        if(!isCoordInInput(y, x)){
            HashMap<Integer, Integer> distanceToCoords = new HashMap<>();
            for(int i = 0; i < inputCoords.size(); i++){
                int orig_x = Integer.parseInt(inputCoords.get(i).split(", ")[0]);
                int orig_y = Integer.parseInt(inputCoords.get(i).split(", ")[1]);
                distanceToCoords.put(i, abs(((orig_x - x) + (orig_y + y))));
            }

            int closest = Integer.MAX_VALUE;
            int closestCount = 0;
            for(Integer key: distanceToCoords.keySet()){
                if(closest > distanceToCoords.get(key)){
                    closest = distanceToCoords.get(key);
                }
            }

            for (Integer i: distanceToCoords.keySet()
                 ) {
                if(distanceToCoords.get(i).equals(closest)){
                    closestCount++;
                }
            }

            if(closestCount > 1){
                closest = -1;
            }

            return Integer.toString(closest);

        } else {
            return grid.get(y).get(x);
        }
    }

    private boolean isCoordInInput(int y, int x) {
        for (String input: inputCoords
             ) {
            int orig_x = Integer.parseInt(input.split(", ")[0]);
            int orig_y = Integer.parseInt(input.split(", ")[1]);

            if(orig_x == x && orig_y == y){
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
        Ex ex = new Ex();
        ex.init();
        ex.createGrid();
        ex.placeCoords();
        ex.manhattanProject();
        now = LocalDateTime.now();
        System.out.println(dtf.format(now));
    }
}
